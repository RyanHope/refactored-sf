from __future__ import division
import pygame
import os
import sys
import time
import sounds
import game
import screen
import log
import config
import missile
import drawing
from experiment import exp
from assets import Assets

import pygame_objects as pyobj

class SDLGame(game.Game, screen.Screen):
    def __init__(self, conf, game_name, game_number):
        screen.Screen.__init__(self, 'game')
        game.Game.__init__(self, conf, game_name, game_number)
        # input
        self.key_bindings = {eval("pygame.K_%s"%self.config["key_bindings"]["thrust"]): 'thrust',
                             eval("pygame.K_%s"%self.config["key_bindings"]["left"]): 'left',
                             eval("pygame.K_%s"%self.config["key_bindings"]["right"]): 'right',
                             eval("pygame.K_%s"%self.config["key_bindings"]["fire"]): 'fire',
                             eval("pygame.K_%s"%self.config["key_bindings"]["IFF"]): 'iff',
                             eval("pygame.K_%s"%self.config["key_bindings"]["shots"]): 'shots',
                             eval("pygame.K_%s"%self.config["key_bindings"]["pnts"]): 'pnts'}

        # self.keys_pressed = {self.thrust_key:False, self.left_turn_key:False,
        #                      self.right_turn_key:False, self.fire_key:False,
        #                      self.IFF_key:False, self.shots_key:False,
        #                      self.pnts_key:False}
        # audio
        self.sounds_enabled = int(self.config['sounds']) == 1
        # graphics
        self.load_display_assets()
        r = self.screen.get_rect()

        self.SCREEN_WIDTH = r.width
        self.SCREEN_HEIGHT = r.height
        self.worldsurf = pygame.Surface((self.WORLD_WIDTH, self.WORLD_HEIGHT))
        self.worldrect = self.worldsurf.get_rect()
        self.worldrect.top = 5
        self.worldrect.centerx = self.SCREEN_WIDTH/2
        self.scoresurf = pygame.Surface((self.WORLD_WIDTH, 64))
        self.scorerect = self.scoresurf.get_rect()
        self.scorerect.top = 634
        self.scorerect.centerx = self.SCREEN_WIDTH/2
        #
        self.clock = pygame.time.Clock()

    def load_display_assets (self):
        self.screen = pygame.display.get_surface()
        self.f = Assets.f
        self.f24 = Assets.f24
        self.f28 = Assets.f28
        self.f96 = Assets.f96
        self.f36 = Assets.f36
        self.vector_explosion = Assets.vector_explosion
        self.vector_explosion_rect = self.vector_explosion.get_rect()

    def open_logs(self):
        self.log.open_gamelogs(self.config)

    def delay(self, ms):
        pygame.time.delay(ms)

    def now(self):
        return time.time()

    def play_sound(self, sound_id):
        if self.sounds_enabled:
            sounds.Sounds.play(sound_id)

    def draw_world(self):
        self.screen.fill((0,0,0))
        self.worldsurf.fill((0,0,0))
        self.scoresurf.fill((0,0,0))
        # Draws the game boundary.
        pygame.draw.rect(self.worldsurf, (0,255,0), (0,0, 710, 625), 1)
        for shell in self.shell_list:
            pyobj.draw_shell(shell, self.worldsurf)
        for missile in self.missile_list:
            pyobj.draw_missile(missile, self.worldsurf)
        #draws a small black circle under the fortress so we don't see the shell in the center
        pygame.draw.circle(self.worldsurf, (0,0,0), (355,315), 30)
        if self.fortress.exists:
            if self.fortress.alive:
                pyobj.draw_fortress(self.fortress, self.worldsurf)
            else:
                self.vector_explosion_rect.center = (self.fortress.position.x, self.fortress.position.y)
                self.worldsurf.blit(self.vector_explosion, self.vector_explosion_rect)
        if self.ship.alive:
            pyobj.draw_ship(self.ship, self.worldsurf)
        else:
            self.vector_explosion_rect.center = (self.ship.position.x, self.ship.position.y)
            self.worldsurf.blit(self.vector_explosion, self.vector_explosion_rect)
        pyobj.draw_score(self.score, self.scoresurf)
        pyobj.draw_hex(self.bighex, self.worldsurf)
        pyobj.draw_hex(self.smallhex, self.worldsurf)
        if self.mine.alive:
            pyobj.draw_mine(self.mine, self.worldsurf)
        if self.bonus.exists and self.bonus.visible:
            pyobj.draw_bonus(self.bonus, self.worldsurf)
        self.screen.blit(self.worldsurf, self.worldrect)
        self.screen.blit(self.scoresurf, self.scorerect)
        if int(self.config['draw_key_state']) == 1:
            self.draw_key_state()
        if int(self.config['draw_auto_state']) == 1:
            if self.ship.auto_turn:
                drawing.draw_tag(self.screen, 165, 20, "Turn", True, font=self.f)
            if self.ship.auto_thrust:
                drawing.draw_tag(self.screen, 230, 20, "Thrust", True, font=self.f)
        if self.wait_for_player == True:
            ms = 1500
            i = 105 + abs(int((self.gameTimer.time % ms) / ms * 300) - 150)
            c = (i,i,i)
            drawing.blit_text(self.screen, self.f, "Press a key to start", color=c, y=425, valign='center', halign='center')
        pygame.display.flip()

    def get_event(self):
        return pygame.event.get()

    def reset_event_queue(self):
        pygame.event.clear()

    def is_caret_key(self,event):
        return event.key == pygame.K_CARET or (event.key == pygame.K_6 and
                                               (event.mod&pygame.KMOD_LSHIFT or
                                                event.mod&pygame.KMOD_RSHIFT or
                                                event.mod&pygame.KMOD_SHIFT))

    def debug_set_sounds(self, val):
        self.sounds_enabled = val

    def exit_prematurely(self):
        self.log.log_premature_exit()
        self.log.close_gamelogs()
        sys.exit()

    def process_input_events(self):
        """chief function to process keyboard events"""
        self.key_state.reset_tick()
        keys = []
        events = self.get_event()
        for event in events:
            if exp.handle_event(event):
                pass
            else:
                if event.type == pygame.KEYDOWN or event.type == pygame.KEYUP:
                    keys.append([event.type, event.key, event.mod])
                if event.type == pygame.KEYDOWN:
                    # self.keys_pressed[event.key] = True
                    if self.is_caret_key(event):
                        self.log.add_event('got-caret')
                    elif event.key in self.key_bindings:
                        self.press_key(self.key_bindings[event.key])
                    elif event.key == pygame.K_0:
                        self.ship.auto_turn = not self.ship.auto_turn
                    elif event.key == pygame.K_9:
                        self.ship.auto_thrust = not self.ship.auto_thrust
                    elif event.key == pygame.K_8:
                        self.ship.auto_thrust_debug = not self.ship.auto_thrust_debug
                if event.type == pygame.KEYUP:
                    # self.keys_pressed[event.key] = False
                    if event.key in self.key_bindings:
                        self.release_key(self.key_bindings[event.key])
        b = [self.tinc]
        b.append(keys)
        self.log.write_keys(b)

    def finish(self):
        super(self.__class__, self).finish()
        exp.bonus += self.money

    def run(self):
        self.load_display_assets()
        self.screen.fill((0,0,0))
        self.start()
        exp.slog('start',{'name': self.game_name,
                          'game-number': self.game_number})
        time = pygame.time.get_ticks()
        while True:
            now = pygame.time.get_ticks()
            self.gameTimer.tick(now-time)
            time = now
            self.step_one_tick()
            self.clock.tick(self.fps)
            if self.cur_time >= int(self.config["game_time"]):
                break
        self.finish()
        exp.slog('end')
