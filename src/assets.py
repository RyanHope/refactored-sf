import pkg_resources
import pygame

class Assets(object):
    def __init__(self):
        pass
    @staticmethod
    def load(fp):
        Assets.f = pygame.font.Font(fp, 14)
        Assets.f24 = pygame.font.Font(fp, 20)
        Assets.f28 = pygame.font.Font(fp, 28)
        Assets.f96 = pygame.font.Font(fp, 72)
        Assets.f36 = pygame.font.Font(fp, 36)
        Assets.vector_explosion = pygame.image.load(pkg_resources.resource_stream("resources","gfx/exp.png")).convert()
        Assets.vector_explosion.set_colorkey((0, 0, 0))
