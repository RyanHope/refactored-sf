import model_server

if __name__ == '__main__':
    s = model_server.Server(model_server.Logger(), model_server.TrivialDrawState)
    s.handle_connections()
