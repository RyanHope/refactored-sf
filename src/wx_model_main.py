import math
import wx
import threading

import model_server

class Object2D(object):
    def __init__(self, points, lines):
        self.points = points
        self.lines = lines

    def rotate_and_translate_point(self, p, x, y, angle):
        s = math.sin(math.radians(angle))
        c = math.cos(math.radians(angle))
        return (p[0]*c - p[1]*s + x, -p[0]*s - p[1]*c + y)

    def rotate_and_translate_points(self, x, y, angle):
        return [self.rotate_and_translate_point(p, x, y, angle) for p in self.points]

    def draw(self, dc, x, y, angle):
        p = self.rotate_and_translate_points(x, y, angle)
        for l in self.lines:
            last = l[0]
            for i in l[1:]:
                dc.DrawLine(p[last][0], p[last][1], p[i][0], p[i][1])
                last = i

    def draw_dict(self, dc, obj):
        self.draw(dc, obj['x'], obj['y'], obj['orientation'])

class State(wx.Panel):
    def __init__(self, parent, id):
        wx.Panel.__init__(self, parent, id, size = (410, 450))
        self.parent = parent
        self.state = None
        self.display_level = 0
        self.Bind(wx.EVT_PAINT, self.OnPaint)
        self.Bind(wx.EVT_SIZE, self.OnSize)

        self.font = wx.Font(18, wx.FONTFAMILY_SWISS, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False)

        self.bighex = self.hexagonPoints(200)
        self.smallhex = self.hexagonPoints(40)
        self.ship = Object2D([(-18,0), (18,0), (0,0), (-18,18), (-18,-18)], [[0,1], [3, 2, 4]])
        self.fortress = Object2D([(0, 0), (36, 0), (18, -18), (0,-18), (18,18), (0,18)],
                                 [[0, 1], [3,2,4,5]])
        self.missile = Object2D([(0, 0), (-25, 0), (-5, 5), (-5, -5)],
                                [[0,1], [0, 2], [0, 3]])
        self.shell = Object2D([(-8, 0), (0, -6), (16, 0), (0, 6), (-8, 0)],
                              [[0,1,2,3,0]])
        self.explosion = wx.Bitmap('gfx/exp.png')
        self.explosion.SetMaskColour('#000000')

    def hexagonPoints(self, radius):
        x1 = int(-radius)
        x2 = int(-radius * 0.5)
        x3 = int(radius * 0.5)
        x4 = int(radius)
        y1 = 0
        y2 = int(-radius * math.sin(math.pi*2/3))
        y3 = int(radius * math.sin(math.pi*2/3))
        points = [0] * 7
        points[0] = (x1, y1)
        points[1] = (x2, y2)
        points[2] = (x3, y2)
        points[3] = (x4, y1)
        points[4] = (x3, y3)
        points[5] = (x2, y3)
        points[6] = (x1, y1)
        return Object2D(points, [[0,1,2,3,4,5,6,0]])

    def drawHexagon(self, dc, points):
        dc.SetPen(wx.Pen('#00FF00'))
        dc.DrawLines(points)

    def drawCenteredText(self, dc, text, x, y):
        w, h = dc.GetTextExtent(text)
        dc.DrawText(text, x-w/2, y-h/2)

    def drawCenteredBitmap (self, dc, bm, obj):
        w, h = bm.GetSize()
        dc.DrawBitmap(bm, obj['x']-w/2, obj['y']-h/2, True)

    def drawGameState(self, dc):
        w = 710
        h = 626
        dc.SetDeviceOrigin(-150, -130)
        dc.SetPen(wx.Pen('#D4D4D4'))
        dc.SetBrush(wx.Brush('#000000'))
        dc.DrawRectangle(0, 0, 710, 626)

        dc.SetBrush(wx.Brush('#FFFF00'))
        # dc.DrawRectangle(0, 0, 600, 500)
        dc.SetPen(wx.Pen('#00FF00'))
        self.smallhex.draw(dc, 355, 315, 0)
        self.bighex.draw(dc, 355, 315, 0)

        # dc.SetPen(wx.Pen('#FFFF00'))
        # self.ship.draw(dc, 200, 315, 357)
        # self.fortress.draw(dc, 355, 315, 170)
        # self.missile.draw(dc, 255, 315, 45)
        # self.shell.draw(dc, 255, 365, 200)


        label_width = 89
        pad = 16
        start = (w-label_width*2)/2
        score_y = 520
        dc.SetBrush(wx.TRANSPARENT_BRUSH)
        dc.DrawRectangle(start, score_y, label_width*2, 32)
        dc.DrawLine(start+89,score_y,start+89,score_y+32-1)

        dc.SetFont(self.font)

        if not self.state['active']:
            dc.SetTextForeground('#FFFFFF')
            self.drawCenteredText(dc, 'Press a key to start', w/2, 380)

        dc.SetTextForeground('#FFFF00')
        self.drawCenteredText(dc, str(self.state['pnts']), start+89/2, score_y+16)
        self.drawCenteredText(dc, str(self.state['vlner']), start+89/2 + 89, score_y+16)

        dc.SetPen(wx.Pen('#FFFF00'))
        if self.state['ship']['alive']:
            self.ship.draw_dict(dc, self.state['ship'])
        else:
            self.drawCenteredBitmap(dc, self.explosion, self.state['ship'])

        if self.state['fortress'] != None:
            if self.state['fortress']['alive']:
                self.fortress.draw_dict(dc, self.state['fortress'])
            else:
                self.drawCenteredBitmap(dc, self.explosion, self.state['fortress'])

        dc.SetPen(wx.Pen('#FFFFFF'))
        for m in self.state['missiles']:
            self.missile.draw_dict(dc, m)

        dc.SetPen(wx.Pen('#FF0000'))
        for s in self.state['shells']:
            self.shell.draw_dict(dc, s)

    def drawScreenState(self, dc, name):
        w,h = self.GetSize()
        dc.SetBrush(wx.Brush('#000000'))
        dc.DrawRectangle(0, 0, w, h)
        dc.SetFont(self.font)
        dc.SetTextForeground('#FFFF00')
        self.drawCenteredText(dc, name, w/2, h/2)

    def OnPaint(self, event):
        dc = wx.PaintDC(self)
        dc.Clear()
        if self.state == None:
            self.drawScreenState(dc, 'No Information')
        else:
            if self.state['screen-type'] == 'game':
                self.drawGameState(dc)
            else:
                self.drawScreenState(dc, self.state['screen-type'])

    def OnSize(self, event):
        self.Refresh()

class StateFrame(wx.Frame):
    def __init__(self):
        wx.Frame.__init__(self, None, -1, "Space Fortress", size = (409, 450), style = wx.DEFAULT_FRAME_STYLE & ~(wx.RESIZE_BORDER | wx.MAXIMIZE_BOX))
        panel = wx.Panel(self)
        self.state_panel = State(self, -1)
        self.Bind(wx.EVT_CLOSE, self.close)
        self.Center()

    def close(self, e):
        e.Veto()
        self._set_display_level(0)

    def set_objects(self, objects):
        wx.CallAfter(self._set_objects, objects)

    def _set_objects(self, objects):
        self.state_panel.state = objects
        if self.state_panel.display_level > 0:
            self.state_panel.Refresh()

    def set_display_level(self, level):
        wx.CallAfter(self._set_display_level, level)

    def _set_display_level(self, level):
        if level != self.state_panel.display_level:
            self.state_panel.display_level = level
            if level == 0:
                self.Hide()
            if level == 1:
                self.Show(True)
                self.Raise()
            elif level == 2:
                self.Show(True)
                self.Raise()

    def cleanup(self):
        wx.CallAfter(self.Destroy)

class Logger(object):
    def __init__(self, window):
        self.window = window

    def log(self, str):
        wx.CallAfter(self.window.message, str)


# Jump through hoops to get a StateFrame object to model_server
def MakeStateFrame(c, ret):
    c.acquire()
    ret[0] = StateFrame()
    c.notify()
    c.release()

def createStateFrame():
    c = threading.Condition()
    ret = [None]
    c.acquire()
    wx.CallAfter(MakeStateFrame, c, ret)
    c.wait()
    return ret[0]

class ServerThread(model_server.Server, threading.Thread):
    def __init__(self, window):
        threading.Thread.__init__(self)
        model_server.Server.__init__(self, Logger(window), createStateFrame)


    def run(self):
        self.handle_connections()

class HUD(wx.Frame):
    def __init__(self):
        super(self.__class__, self).__init__(None, -1, "Space Fortress Model Server", size = (700, 400))
        self.server = ServerThread(self)
        self.server.setDaemon(True)
        self.server.start()

        panel = wx.Panel(self)

        lbl = wx.StaticText(panel, label="Server output:")
        but = wx.Button(panel, label="Quit")
        but.Bind(wx.EVT_BUTTON, self.close)

        self.text = wx.TextCtrl(panel, style=wx.TE_MULTILINE|wx.HSCROLL)

        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(lbl, 0, wx.ALL, 5)
        sizer.Add(self.text, 1, wx.EXPAND|wx.ALL, 5)
        sizer.Add(but, flag=wx.BOTTOM|wx.LEFT, border=5)
        panel.SetSizer(sizer)

        self.Bind(wx.EVT_CLOSE, self.close)

        menubar = wx.MenuBar()
        # filemenu = wx.Menu()
        # fitem = filemenu.Append(wx.ID_EXIT, 'Quit', 'Quit Application')
        # menubar.Append(filemenu, '&File')
        self.SetMenuBar(menubar)
        # self.Bind(wx.EVT_MENU, self.close, fitem)

        self.Center()
        self.Show(True)

    def close(self, e):
        self.Destroy()


    def message(self, data):
        # self.text.SetInsertionPointEnd()
        self.text.AppendText(data + '\n')

def start_wx_model_server():
    app = wx.App()
    frame = HUD()
    app.MainLoop()

if __name__ == '__main__':
    start_wx_model_server()
